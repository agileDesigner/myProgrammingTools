# myProgrammingTools

A collection of programs combined into one command to help make programming more efficient.

The current main branch will likely only work with bash. The program should work on all other platforms however the installation (and uninstallation) script will only work on bash.

**See [the wiki](/agileDesigner/myProgrammingTools/wiki) for information on contributing to myProgrammingTools.**

myProgrammingTools requires Python to be installed.

## Installing and uninstalling

At the moment the best way to download this project is to clone the git repo using:

`git clone https://codeberg.org/agiledesigner/myProgrammingTools.git` in your terminal.

Alternatively you can download the repository from the codeberg page and uncompress it manually.

Refer to the README text file (not this README.md) for installation and uninstallation guidance once you have downloaded myProgrammingTools.

## Install troubleshooting

#### Error: `/usr/bin/env: 'python3` No such file or directory"

Steps to fix:

Try the commands `python` and `python3` in your terminal.

If `python3` works then I don't know why your error is occuring. Sorry.

If `python` works try editing the first line of "install", "src/mpt" and "uninstall" (so you don't get the same error should you attempt to uninstall the program)

Change the first line of all three files to `/usr/bin/env python`. Try running `./install` again.

If neither work you probably need to [install Python](https://www.python.org/downloads/) *(external link)*.

#### Error: `bash: ./install: Permission denied`

Steps to fix:

Run the command `chmod +x install && chmod +x src/mpt && chmod +x uninstall`.

Try running `./install` again.
