def h(detailed=False):
    print("albp: auto LBP\nAutomatically removes all styles, links, headers and footers.")
    if detailed:
        print("\nUsage: mpt albp [file]\nPython file: funcManagers/functions/autoLPB.py\nManager file: funcManagers/autoLBPM.py")

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt albp' requires one argument. See 'mpt albp --help'.")
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt albp HELP")
            h(True)
        else:
            from .functions import autoLBP as albp
            if argsNo > 3:
                print("Too many arguments for 'mpt albp'. Using first one.")
            albp.main(arg2)
