def h(detailed=False):
    print("fao: Find all occurences\nFinds all line numbers where an inputted string occurs in a file")
    if detailed:
        print("\nUsage: mpt fao [string to find] [filename/path] [number of lines to skip (Optional)]\nPython file: funcManagers/functions/findAllOccurences.py\nManager file: funcManagers/findAllOccurencesM.py")

def tooFew():
    print("'mpt fao' requires two arguments. See 'mpt fao --help'")

def checkArgs(argv, argsNo):
    if argsNo == 2:
        tooFew()
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt fao HELP")
            h(True)
        elif argsNo == 3:
            tooFew()
        else:
            from .functions import findAllOccurences as fao
            if argsNo == 4:
                fao.main(arg2, argv[3])
            else:
                if argsNo > 5:
                    print("Too many arguments for 'mpt fao'. Trying first three.")
                if argv[4].isdigit():
                    fao.main(arg2, argv[3], int(argv[4]))
                else:
                    print("Skip value must be an integer. Trying first two arguments.")
                    fao.main(arg2, argv[3])
