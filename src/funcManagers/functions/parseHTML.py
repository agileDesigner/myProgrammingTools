## Created by agileDesigner
## Find me at https://www.codeberg.org/agiledesginer

class tag():
    def __init__(self, tagname):
        self.name = tagname
        self.startStack = []
        self.lonelyEndTags = []
        self.bounds = []

    def foundStartTag(self, lineNo, characterNo):
        self.startStack.append((lineNo, characterNo))

    def foundEndTag(self, lineNo, characterNo):
        if len(self.startStack) == 0:
            self.lonelyEndTags.append((lineNo, characterNo))
        else:
            self.bounds.append([self.startStack.pop(), (lineNo, characterNo)])

def addToTag(tag, startTag, line, character):
    if startTag:
        tag.foundStartTag(line, character)
    else:
        tag.foundEndTag(line, character)
    return tag

def checkTag(tagName, listOfTags, l, tagOpeningC):
    startTag = True
    if tagName[0] == "/":
        startTag = False
        tagName = tagName[1:]
    newTagNeeded = True
    for t in range(0, len(listOfTags)):
        if listOfTags[t].name == tagName:
            newTagNeeded = False
            listOfTags[t] = addToTag(listOfTags[t], startTag, l, tagOpeningC)
            break
    if newTagNeeded:
        listOfTags.append(tag(tagName))
        tagPos = len(listOfTags) - 1
        listOfTags[tagPos] = addToTag(listOfTags[tagPos], startTag, l, tagOpeningC)
    return listOfTags

def parse(listOfStrings):
    listOfTags = []
    for l in range(0, len(listOfStrings)):
        ## Newline resets tags as tags can't span newlines
        tag = False
        tagNameFinished = False
        tagOpeningC = 0
        tagName = ""
        for c in range(0, len(listOfStrings[l])):
            character = listOfStrings[l][c]
            if character == "<":
                tag = True
                tagOpeningC = c
            elif tag is True:
                if character == ">":
                    listOfTags = checkTag(tagName, listOfTags, l, tagOpeningC)
                    tag = False
                    tagName = ""
                    tagOpeningC = 0
                    tagNameFinished = False
                elif tagNameFinished:
                    pass
                elif character == " ":
                    tagNameFinished = True
                else:
                    tagName += character
            else:pass
    return listOfTags
