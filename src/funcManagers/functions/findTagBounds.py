## Created by agileDesigner
## Find me at https://www.codeberg.org/agiledesigner

from .import parseHTML as pHTML
from .readFile import readFile as rF
from pathlib import Path
from os import listdir, getcwd

## Checks if inputted directory exists
def checkDir(directory):
    path = Path(directory)
    if path.is_dir():
        return True
    else:
        return False

def getFilesList(directory, fileExt):
    dirList = listdir(directory)
    filesList = []
    for item in dirList:
        if item[-len(fileExt):] == fileExt:
            filesList.append(item)
    return filesList

def printInfo(tagObj):
    for bound in tagObj.bounds:
        print(f"{str(bound[0][0]+1)}, {bound[0][1]+1} to {str(bound[1][0]+1)}, {bound[1][1]+1}")
    for startTag in tagObj.startStack:
        print(f"Start tag with no end tag at {str(startTag[0]+1)}, {startTag[1]+1}")
    for endTag in tagObj.lonelyEndTags:
        print(f"End tag with no start at {str(endTag[0]+1)}, {endTag[1]+1}")

def checkTagBounds(pageContent, tag, verbose):
    if verbose:
        print("Parsing page...")
    listOfTags = pHTML.parse(pageContent)
    if tag != "":
        if verbose:
            print(f"Searching for tag object entitled '{tag}'...")
        foundTag = False
        for tagObj in listOfTags:
            if tagObj.name == tag:
                printInfo(tagObj)
                foundTag = True
                break
        if foundTag is False:
            print(f"The tag {tag} was not found.")
        return
    for tagObj in listOfTags:
        print(tagObj.name)
        printInfo(tagObj)

def loopThroughPages(filesList, directory, tag, verbose):
    for page in filesList:
        if verbose:
            print(f"\nReading {page}...")
        else:
            print("")
        pageContent = rF(f"{directory}/'{page}")
        print(f"For page: {page}")
        checkTagBounds(pageContent, tag, verbose)

def main(tag="", fileExt=".html", directory=getcwd(), verbose=False):
    if checkDir(directory):
        if fileExt[0] != ".":
            fileExt = "." + fileExt
        if verbose:
            print("Getting file list...")
        filesList = getFilesList(directory, fileExt)
        if len(filesList) == 0:
            print(f"No {fileExt} files found in {directory}")
        else:
            if tag == "":
                print(f"All tag bounds for {fileExt} files in {directory}")
            else:
                print(f"\n{tag} bounds for {fileExt} files in {directory}")
            loopThroughPages(filesList, directory, tag, verbose)
    else:
        print(f"Directory {directory} couldn't be found")
