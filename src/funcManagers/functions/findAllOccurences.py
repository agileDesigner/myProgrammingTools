## Created by agileDesigner
## Find me at https://codeberg.org/agiledesigner

from .isFile import isFile as iF
from .readFile import readFile as rF

## Checks each string in a list of strings for stringToFind
## Returns empty array if no occurences are found
def findAllMentions(stringToFind, listOfStrings, skip):
    occurences = []
    for i in range(skip, len(listOfStrings)):
        if stringToFind in listOfStrings[i]:
            occurences.append(i)
    return occurences

def main(stringToFind, filePath, skip=0):
    if !iF(filePath):
        print(f"File 'filePath' could not be found")
        return
    occurences = findAllMentions(stringToFind, rF(filePath) , skip)
    print(filePath)
    if occurences == []:
        print(f"No occurences of {stringToFind} found.")
    else:
        print(f"'{stringToFind}' found on lines:")
        for occurence in occurences:
            print(occurence+1, end="  ")
        print()
        print(f"{len(occurences)} occurence/s total.")
