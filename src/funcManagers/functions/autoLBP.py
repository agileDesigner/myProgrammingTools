from .import parseHTML as pHTML
from .isFile import isFile as iF
from .readFile import readFile as rF
from .isSelfClosingTag import isSCT
from pathlib import Path
from os import mkdir

def writeLBPFile(fileName, fileContent, linesRemoved):
    lbpPath = Path("lbp")
    writeFile = lbpPath.is_dir()
    while not writeFile:
        createDir = input("'lbp' directory could not be found. Create it (y/n)? ").strip().lower()
        if createDir == 'y':
            mkdir("lbp")
            writeFile = True
        elif createDir == 'n':
            print("'lbp' directory not found, process aborted.")
            break
        else:
            print(f"{createDir} not recognised, enter either 'y' or 'n'.")
    if writeFile:
        with open(f"lbp/{fileName}", "w") as LBPFile:
            LBPFile.writelines(fileContent)
        print(f"{linesRemoved} lines were removed to make the LBP file")
        print(f"LBP file written to lbp/{fileName}")

def markLinesBetween(start, end, targetList):
    for i in range(end-1, start, -1):
        targetList[i] = ""
    return targetList

def findTagEnd(tagStart, string):
    while string[tagStart] != ">":
        tagStart += 1
    return tagStart

def tagIsOnlyContent(line):
    onlyTag = True
    tag = False
    cleanedLine = line.strip()
    for c in range(0, len(cleanedLine)):
        if tag:
            if cleanedLine[c] == ">":
                tag = False
        else:
            if cleanedLine[c] == "<":
                tag = True
            else:
                onlyTag = False
                break
    return onlyTag

def markSections(boundsList, pageContent):
    for bound in boundsList:
        ## (l,c),(l,c)
        startTagLineNo = bound[0][0]
        endTagLineNo = bound[1][0]
        if startTagLineNo == endTagLineNo:
            line = pageContent[startTagLineNo]
            ## Takes characters either side of the tag and concatenates them
            pageContent[startTagLineNo] = line[:bound[0][1]] + line[findTagEnd(bound[1][1], line)+1:]
        else:
            if tagIsOnlyContent(pageContent[startTagLineNo]):
                pageContent[startTagLineNo] = ""
            else:
                ## Removes all characters after start tag on that line + the tag
                pageContent[startTagLineNo] = pageContent[startTagLineNo][:bound[0][1]]
            if tagIsOnlyContent(pageContent[endTagLineNo]):
                pageContent[endTagLineNo] = ""
            else:
                ## Removes all characters before end tag on that line + the tag
                pageContent[endTagLineNo] = pageContent[endTagLineNo][findTagEnd(bound[1][1], pageContent[endTagLineNo])+1:]
            ## Mark lines between bound's lines
            pageContent = markLinesBetween(startTagLineNo, endTagLineNo, pageContent)
    return pageContent

def markSingleTag(startStack, pageContent):
    for tag in startStack:
        tagLineNo = tag[0]
        tagCharNo = tag[1]
        ## Takes characters either side of the tag and concatenates them
        pageContent[tagLineNo] = pageContent[tagLineNo][:tagCharNo] + pageContent[tagLineNo][findTagEnd(tagCharNo, pageContent[tagLineNo])+1:]
    return pageContent

def conditionalMarkSections(tag, tagPosDict, listOfTags, pageContent):
    if tag in tagPosDict:
        if isSCT(tag):
            ## Self-closing tag
            pageContent = markSingleTag(listOfTags[tagPosDict[tag]].startStack, pageContent)
        else:
            pageContent = markSections(listOfTags[tagPosDict[tag]].bounds, pageContent)
    else:
        print(f"No {tag} tags found in page")
    return pageContent

def getTagPosDict(listOfTags):
    ## Returns dictionary of all the tag positions in listOfTags in form {"tagname":position}
    tagPosDict = {}
    for i in range(0, len(listOfTags)):
        tagPosDict[listOfTags[i].name] = i
    return tagPosDict

def removeMarked(pageContent, preBounds=[]):
    protectedLines = []
    for bound in preBounds:
        ## (l,c),(l,c)
        for l in range(bound[0][0], bound[1][0]):
            protectedLines.append(l)
    for line in range(len(pageContent)-1, -1, -1):
        if line not in protectedLines:
            if pageContent[line].strip() == "":
                del pageContent[line]
            else:
                ## Removes unnecessary whitespace at beginning or end of lines
                ## Doesn't remove \n's as they're treated differently
                ## by website renderers
                pageContent[line] = pageContent[line].strip(" \t")
    return pageContent

def main(fileName):
    if iF(fileName):
        pageContent = rF(fileName)
        listOfTags = pHTML.parse(pageContent)
        numOfLines = len(pageContent)
        tagPosDict = getTagPosDict(listOfTags)

        ## Instead of trying to keep track of what lines were removed
        ## Any lines that are to be removed are made empty (="")
        ## Then reparse the new page and remove all empty lines not in a <pre>
        ## This is also good as it removes any unessecary lines that have been
        ## left in during development

        ## Find style bounds and marks section for removal
        pageContent = conditionalMarkSections("style", tagPosDict, listOfTags, pageContent)

        ## ^ but for header
        pageContent = conditionalMarkSections("header", tagPosDict, listOfTags, pageContent)

        ## ^ but for footer
        pageContent = conditionalMarkSections("footer", tagPosDict, listOfTags, pageContent)

        ## ^ but for link
        pageContent = conditionalMarkSections("link", tagPosDict, listOfTags, pageContent)

        listOfTags = pHTML.parse(pageContent)
        tagPosDict = getTagPosDict(listOfTags)
        if "pre" in tagPosDict:
            pageContent = removeMarked(pageContent, listOfTags[tagPosDict["pre"]].bounds)
        else:
            pageContent = removeMarked(pageContent)

        ## Write to file with same name in lbp folder
        writeLBPFile(fileName, pageContent, numOfLines - len(pageContent))
    else:
        print(f"File '{fileName}' could not be found")
