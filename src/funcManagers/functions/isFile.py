## Created by agileDesigner
## Find me at https://codeberg.org/agiledesigner

from pathlib import Path

def isFile(filePath):
    path = Path(filePath)
    return path.is_file()

def main(filePath):
    if isFile(filePath):
        print("True")
    else:
        print("False")
