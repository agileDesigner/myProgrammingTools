## Created by agileDesigner
## Find me at https://codeberg.org/agiledesigner

from .isFile import isFile as iF

def readFile(filePath):
    with open(filePath, "r") as openFile:
        listOfLines = openFile.readlines()
    return listOfLines

def main(filePath, lineNums=False):
    if iF(filePath):
        listOfLines = readFile(filePath)
        if lineNums:
            l = 0
            digits = str(len(str(len(listOfLines))))
        for line in listOfLines:
            if lineNums:
                l += 1
                ## If you know a better way of implementing this please let me know
                print(("{:0"+digits+"}").format(l)+"|", end="")
            print(line, end="")
    else:
        print(f"File at '{filePath}' could not be found")
