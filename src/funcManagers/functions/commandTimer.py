from time import perf_counter
from os import system

def main(command):
    start = perf_counter()
    system(command)
    end = perf_counter()
    print(f"Command completed in {end - start} seconds")
