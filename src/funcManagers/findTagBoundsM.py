def h(detailed=False):
    print("ftb: Find tag bounds\nFinds positions of bounds of tags in all files in a directory with a set file extension.")
    if detailed:
        print("\nUsage: mpt ftb [tag (Optional, outputs all if not inputted)] [file extension (Optional, assumed '.html')] [file directory (Optional, assumed current directory)] [-v (Optional, makes the program output a little more during its process)]\nPython file: funcManagers/functions/findTagBounds.py\nManager file: funcManagers/findTagBoundsM.py")

def checkArgs(argv, argsNo):
    from .functions import findTagBounds as ftb
    if argsNo == 2:
        ftb.main()
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt ftb HELP")
            h(True)
        else:
            if argsNo == 3:
                ftb.main(arg2)
            elif argsNo == 4:
                ftb.main(arg2, argv[3])
            elif argsNo == 5:
                ftb.main(arg2, argv[3], argv[4])
            ## else means argNo >= 6
            else:
                if argsNo > 6:
                    print("Too many arguments for 'mpt ftb'. Trying first four.")
                if argv[5] == "-v":
                    print("Verbose option activated")
                    ftb.main(arg2, argv[3], argv[4], True)
                else: ## ArgsNo == 6 but argv[5] was invalid
                    print(f"{argv[5]} is not a recognised argument. Using first three arguments")
                    ftb.main(arg2, argv[3], argv[4])
