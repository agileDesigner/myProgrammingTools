def h(detailed=False):
    print("rf: Read file\nOutputs inputted file. Does the same job as commands such as 'cat' but has less features.")
    if detailed:
        print("\nUsage: mpt rf [filename] [-n (Optional, shows line numbers)]\nPython file: funcManagers/functions/readFile.py\nManager file: funcManagers/readFileM.py")

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt rf' requires at least one argument. See 'mpt rf --help'")
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt rf HELP")
            h(True)
        else:
            from .functions import readFile as rf
            if argsNo == 3:
                rf.main(arg2)
            else:
                ## argsNo >= 4
                if argsNo > 4:
                    print("Too many arguments for 'mpt rf'. Trying first two.")
                if argv[3] == "-n":
                    rf.main(arg2, True)
                else:
                    print(f"Argument {argv[3]} is not valid. Using first argument.")
                    rf.main(arg2)
            
