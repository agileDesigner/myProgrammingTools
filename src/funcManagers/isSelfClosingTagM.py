def h(detailed=False):
    print("isct: Is self closing tag\nTells you if entered tag is self closing or not.")
    if detailed:
        print("\nUsage: mpt isct [tagname]\nPython file: funcManagers/functions/isSelfClosingTag.py\nManager file: funcManagers/isSelfClosingTag.py")

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt isct' requires one argument. See 'mpt isct --help'")
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt isct HELP")
            h(True)
        else:
            if argsNo > 3:
                print("Too many arguments for 'mpt isct'. Using first one.")
            from .functions import isSelfClosingTag as isct
            isct.main(arg2)
