def h(detailed=False):
    print("if: Is file\nReturns true if inputted path is a file")
    if detailed:
        print("\nUsage: mpt if [filename/path]\nPython file: funcManagers/functions/isFile.py\nManager file: funcManagers/isFileM.py")

def checkArgs(argv, argsNo):
    if argsNo == 2:
        print("'mpt if' requires one argument. See 'mpt if --help'")
    else:
        arg2 = argv[2]
        if arg2 == "-h" or arg2 == "--help":
            print("mpt if HELP")
            h(True)
        else:
            if argsNo > 3:
                print("Too many arguments for 'mpt if'. Using first one.")
            from .functions import isFile as iF
            iF.main(arg2)
